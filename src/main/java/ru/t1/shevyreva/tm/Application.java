package ru.t1.shevyreva.tm;

import ru.t1.shevyreva.tm.constant.ApplicationConst;
import ru.t1.shevyreva.tm.constant.ArgumentConst;
import ru.t1.shevyreva.tm.constant.CommandConst;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        processArguments(args);
        System.out.println("**Welcome to Task Manager**");
        final Scanner scanner = new Scanner(System.in);
        while(!Thread.currentThread().isInterrupted()) {
            System.out.println("Enter command:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        exit();
    }

    public static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command.toLowerCase()) {
            case CommandConst.ABOUT:
                showAbout();
            break;
            case CommandConst.VERSION:
                showVersion();
            break;
            case CommandConst.HELP:
                showHelp();
            break;
            case CommandConst.EXIT:
                exit();
            break;
            default:
                showCommandError();
            break;
        }
    }

    public static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg.toLowerCase()) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showArgumentError();
                break;
        }
    }

    public static void showCommandError() {
        System.err.println("[ERROR]");
        System.err.printf("This command is not supported. Enter command \"%s\" for command list. \n", CommandConst.HELP);
        exitError();
    }

    public static void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.printf("This argument is not supported. Enter argument \"%s\" for command list. \n", ArgumentConst.HELP);
        exitError();
    }

    public static void exit() {
        System.exit(0);
    }

    public static void exitError() {
        System.exit(1);
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Shevyreva Liya");
        System.out.println("e-mail: liyavmax@gmail.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.printf("%s.%s.%s \n", ApplicationConst.APP_MAJOR_VERSION, ApplicationConst.APP_MINOR_VERSION, ApplicationConst.APP_FIXES_VERSION);
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s,%s: Show about program. \n", CommandConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s,%s: Show program version. \n", CommandConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s,%s: Show command list. \n", CommandConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s: Close application. \n", CommandConst.EXIT);
    }

}
